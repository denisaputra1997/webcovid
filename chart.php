<?php
$data = file_get_contents("https://covid19-public.digitalservice.id/api/v1/rekapitulasi_app/jabar_v2");
$json = json_decode($data, true);

?>

<p><h4>Angka Kejadian di Jawa Barat</h4>
<h6>Update Terakhir : <?php echo $json['data']['metadata']['last_update']; ?></h6>
</p>

<div class="row row-cols-1 row-cols-md-4  g-4">
    <div class="col">
        <div class="card border-dark mb-3 shadow p-3 mb-5 bg-body rounded-3" style="max-width: 18rem;">
            <div class="card-body text-center">
                <h5 class="card-title">TERKONFIRMASI</h5>
				<P><h2 class="text-primary"><?php echo $json['data']['content']['confirmation_total']; ?></h2></p>
                <p class="card-text">Total terkonfirmasi</p>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card border-dark mb-3 shadow p-3 mb-5 bg-body rounded-3" style="max-width: 18rem;">
            <div class="card-body text-center">
                <h5 class="card-title">DIISOLASI</h5>
				<P><h2 class="text-warning"><?php echo $json['data']['content']['confirmation_diisolasi']; ?></h2></p>
                <p class="card-text">Total terkonfirmasi diisolasi</p>
            </div>
        </div>
    </div>
	<div class="col">
        <div class="card border-dark mb-3 shadow p-3 mb-5 bg-body rounded-3" style="max-width: 18rem;">
			<div class="card-body text-center">
                <h5 class="card-title">SEMBUH</h5>
				<P><h2 class="text-info"><?php echo $json['data']['content']['confirmation_sembuh']; ?></h2></p>
                <p class="card-text">Total terkonfirmasi sembuh</p>
            </div>
        </div>
    </div>
	<div class="col">
        <div class="card border-dark mb-3 shadow p-3 mb-5 bg-body rounded-3" style="max-width: 18rem;">
            <div class="card-body text-center">
                <h5 class="card-title">MENINGGAL</h5>
				<P><h2 class="text-danger"><?php echo $json['data']['content']['confirmation_meninggal']; ?></h2></p>
                <p class="card-text">Total terkonfirmasi meninggal</p>
            </div>
        </div>
    </div>
</div>