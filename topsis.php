<?php
    include('include/db.php');
    $fetchdata = $database->getReference("Data")->getChild($_GET['user'])->getValue();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Dashboard - Kelola Pengguna</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="Portal - Bootstrap 5 Admin Dashboard Template For Developers">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">
    <link rel="shortcut icon" href="favicon.ico">

    <!-- FontAwesome JS-->
    <script defer src="assets/plugins/fontawesome/js/all.min.js"></script>

    <!-- App CSS -->
    <link id="theme-style" rel="stylesheet" href="assets/css/portal.css">

</head>

<body class="app">

    <div class="app-wrapper">
        <?php include('header.php'); ?>
        <div class="app-content pt-3 p-md-3 p-lg-4">
            <div class="container-xl">

                <div class="row g-3 mb-4 align-items-center justify-content-between">
                    <div class="col-auto">
                        <h1 class="app-page-title mb-0">Kelola Pengguna</h1>
                    </div>
                    <div class="col-auto">
                        <div class="page-utilities">
                            <div class="row g-2 justify-content-start justify-content-md-end align-items-center">
                                <div class="col-auto">
                                    <form class="table-search-form row gx-1 align-items-center">
                                        <div class="col-auto">
                                            <input type="text" id="search-orders" name="searchorders"
                                                class="form-control search-orders" placeholder="Search">
                                        </div>
                                        <div class="col-auto">
                                            <button type="submit" class="btn app-btn-secondary">Search</button>
                                        </div>
                                    </form>

                                </div>
                                <!--//col-->
                                <div class="col-auto">

                                    <select class="form-select w-auto">
                                        <option selected value="option-1">All</option>
                                        <option value="option-2">This week</option>
                                        <option value="option-3">This month</option>
                                        <option value="option-4">Last 3 months</option>

                                    </select>
                                </div>
                                <div class="col-auto">
                                    <a class="btn app-btn-secondary" href="#">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-download me-1"
                                            fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z" />
                                            <path fill-rule="evenodd"
                                                d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z" />
                                        </svg>
                                        Download CSV
                                    </a>
                                </div>
                            </div>
                            <!--//row-->
                        </div>
                        <!--//table-utilities-->
                    </div>
                    <!--//col-auto-->
                </div>
                <!--//row-->

                <div class="tab-content" id="orders-table-tab-content">
                    <div class="tab-pane fade show active" id="orders-all" role="tabpanel"
                        aria-labelledby="orders-all-tab">
                        
                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            <div class="app-card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered app-table-hover mb-0 text-left">
                                        <thead>
                                            <tr>
                                                <?php for ($i=1; $i <= 15; $i++):?>
                                                    <th class="cell">C<?= $i ?></th>
                                                <?php endfor ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                
                                                if (isset($fetchdata['topsis'])) {
                                            ?>
                                                <tr>
                                                <?php for ($i=1; $i <= 15; $i++): ?>
                                                    <td ><?= $fetchdata["topsis"]["c" . $i] ?></td>
                                                <?php endfor ?>
                                                </tr>
                                            <?php } else { ?>
                                                <tr>
                                                    <td colspan="15">Tidak ada Data</td>
                                                </tr>

                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                                <!--//table-responsive-->
                            </div>
                            <!--//app-card-body-->
                        </div>
                        <!--//app-card-->

                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            <div class="app-card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered app-table-hover mb-0 text-left">
                                        <thead>
                                            <tr>
                                                <th class="cell" rowspan="2">ALTERNATIF</th>
                                                <th class="cell" colspan="15">KRITERIA</th>
                                            </tr>
                                            <tr>
                                                <?php for ($i=1; $i <= 15; $i++):?>
                                                    <th class="cell">C<?= $i ?></th>
                                                <?php endfor ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php

                                                if (isset($fetchdata['topsis'])) {
                                                    $gejala_ringan = [0.696310624, 0.588348405, 0.43643578, 0.43643578, 0.377964473, 0.377964473, 0.182574186, 0.39223227, 0.39223227, 0.348155312, 0.324442842, 0.272165527, 0.272165527, 0.272165527, 0.272165527];
                                                    
                                                    $gejala_sedang = [0.348155312, 0.39223227, 0.43643578, 0.43643578, 0.377964473, 0.377964473, 0.730296743, 0.588348405, 0.588348405, 0.696310624, 0.486664263, 0.40824829, 0.40824829, 0.40824829, 0.40824829];
                                                    
                                                    $gejala_berat = [0.522232968, 0.588348405, 0.654653671, 0.654653671, 0.755928946, 0.755928946, 0.547722558, 0.588348405, 0.588348405, 0.522232968, 0.648885685, 0.680413817, 0.680413817, 0.544331054, 0.544331054];
                                                   
                                                    $gejala_kritis = [0.348155312, 0.39223227, 0.43643578, 0.43643578, 0.377964473, 0.377964473, 0.365148372, 0.39223227, 0.39223227, 0.348155312, 0.486664263, 0.544331054, 0.544331054, 0.680413817, 0.680413817];

                                                    $hasil_ringan = [];
                                                    $hasil_sedang = [];
                                                    $hasil_berat = [];
                                                    $hasil_kritis = [];

                                                    $title = ["Gejala Ringan", "Gejala Sedang", "Gejala Berat", "Gejala Kritis"];

                                                    
                                                    for ($i=1; $i <= 15; $i++) { 
                                                        $y = $i - 1;
                                                        $hasil_ringan[$i] = $fetchdata["topsis"]["c" . $i] * $gejala_ringan[$y];
                                                        $hasil_sedang[$i] = $fetchdata["topsis"]["c" . $i] * $gejala_sedang[$y];
                                                        $hasil_berat[$i] = $fetchdata["topsis"]["c" . $i] * $gejala_berat[$y];
                                                        $hasil_kritis[$i] = $fetchdata["topsis"]["c" . $i] * $gejala_kritis[$y];
                                                    }
                                                    
                                                    foreach ($title as $key => $title) {
                                                    
                                            ?>
                                                <tr>
                                                    <td><?= $title ?></td>
                                                    <?php
                                                    switch ($title) {
                                                        case 'Gejala Ringan':
                                                            foreach ($hasil_ringan as $key => $value){
                                                                echo "<td>$value</td>";
                                                            }
                                                            break;
                                                        case 'Gejala Sedang':
                                                            foreach ($hasil_sedang as $key => $value){
                                                                echo "<td>$value</td>";
                                                            }
                                                            break;
                                                        case 'Gejala Berat':
                                                            foreach ($hasil_berat as $key => $value){
                                                                echo "<td>$value</td>";
                                                            }
                                                            break;
                                                        case 'Gejala Kritis':
                                                            foreach ($hasil_kritis as $key => $value){
                                                                echo "<td>$value</td>";
                                                            }
                                                            break;
                                                    }?>
                                                </tr>
                                            <?php }} else { ?>
                                                <tr>
                                                    <td colspan="16">Tidak ada Data</td>
                                                </tr>

                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                                <!--//table-responsive-->
                            </div>
                            <!--//app-card-body-->
                        </div>
                        <!--//app-card-->

                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            <div class="app-card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered app-table-hover mb-0 text-left">
                                        <thead>
                                            <tr>
                                                <th class="cell" colspan="16">MATRIK SOLUSI IDEAL</th>
                                            </tr>
                                            <tr>
                                                <th class="cell" rowspan="2">MATRIK</th>
                                                <th class="cell" colspan="15">KRITERIA</th>
                                            </tr>
                                            <tr>
                                                <?php for ($i=1; $i <= 15; $i++):?>
                                                    <th class="cell">C<?= $i ?></th>
                                                <?php endfor ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php

                                                if (isset($fetchdata['topsis'])) {

                                                    $solusi_ideal_positif = [];
                                                    $solusi_ideal_negatif = [];
                                                    
                                                    for ($i=1; $i <= 15; $i++) {
                                                            $tes = [$hasil_ringan[$i], $hasil_sedang[$i], $hasil_berat[$i], $hasil_kritis[$i]];

                                                            $solusi_ideal_positif[$i] = (max($tes));
                                                            $solusi_ideal_negatif[$i] = (min($tes));
                                                    }

                                                    $solusi_ideal = ["Solusi Ideal A+", "Solusi Ideal A-"];
                                                    
                                                    foreach ($solusi_ideal as $key => $solusi) {
                                                    
                                            ?>
                                                <tr>
                                                    <th class="cell"><?= $solusi ?></th>
                                                    <?php
                                                    switch ($solusi) {
                                                        case 'Solusi Ideal A+':
                                                            foreach ($solusi_ideal_positif as $key => $value){
                                                                echo "<td>$value</td>";
                                                            }
                                                            break;
                                                        case 'Solusi Ideal A-':
                                                            foreach ($solusi_ideal_negatif as $key => $value){
                                                                echo "<td>$value</td>";
                                                            }
                                                            break;
                                                    }?>
                                                </tr>
                                            <?php }} else { ?>
                                                <tr>
                                                    <td colspan="16">Tidak ada Data</td>
                                                </tr>

                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                                <!--//table-responsive-->
                            </div>
                            <!--//app-card-body-->
                        </div>
                        <!--//app-card-->

                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            <div class="app-card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered app-table-hover mb-0 text-left">
                                        <thead>
                                            <tr>
                                                <th class="cell" colspan="4">JARAK MATRIK SOLUSI IDEAL</th>
                                            </tr>
                                            <tr>
                                                <th class="cell">ALTERNATIF</th>
                                                <th class="cell">D+</th>
                                                <th class="cell">D-</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php

                                                if (isset($fetchdata['topsis'])) {

                                                    $jarak_ringan_positif = [];
                                                    $jarak_ringan_negatif = [];
                                                    $jarak_sedang_positif = [];
                                                    $jarak_sedang_negatif = [];
                                                    $jarak_berat_positif = [];
                                                    $jarak_berat_negatif = [];
                                                    $jarak_kritis_positif = [];
                                                    $jarak_kritis_negatif = [];

                                                    for ($i=1; $i <= 15; $i++) {
                                                        $jarak_ringan_positif[$i] = pow(($solusi_ideal_positif[$i] - $hasil_ringan[$i]), 2);
                                                        $jarak_ringan_negatif[$i] = pow(($solusi_ideal_negatif[$i] - $hasil_ringan[$i]), 2);
                                                        $jarak_sedang_positif[$i] = pow(($solusi_ideal_positif[$i] - $hasil_sedang[$i]), 2);
                                                        $jarak_sedang_negatif[$i] = pow(($solusi_ideal_negatif[$i] - $hasil_sedang[$i]), 2);
                                                        $jarak_berat_positif[$i] = pow(($solusi_ideal_positif[$i] - $hasil_berat[$i]), 2);
                                                        $jarak_berat_negatif[$i] = pow(($solusi_ideal_negatif[$i] - $hasil_berat[$i]), 2);
                                                        $jarak_kritis_positif[$i] = pow(($solusi_ideal_positif[$i] - $hasil_kritis[$i]), 2);
                                                        $jarak_kritis_negatif[$i] = pow(($solusi_ideal_negatif[$i] - $hasil_kritis[$i]), 2);
                                                    }

                                                    $jarak_ringan = [sqrt(array_sum($jarak_ringan_positif)), sqrt(array_sum($jarak_ringan_negatif))];
                                                    $jarak_sedang = [sqrt(array_sum($jarak_sedang_positif)), sqrt(array_sum($jarak_sedang_negatif))];
                                                    $jarak_berat = [sqrt(array_sum($jarak_berat_positif)), sqrt(array_sum($jarak_berat_negatif))];
                                                    $jarak_kritis = [sqrt(array_sum($jarak_kritis_positif)), sqrt(array_sum($jarak_kritis_negatif))];

                                                    $jarak = [$jarak_ringan, $jarak_sedang, $jarak_berat, $jarak_kritis];

                                                    $title = ["Gejala Ringan", "Gejala Sedang", "Gejala Berat", "Gejala Kritis"];
                                                    
                                                    foreach ($title as $key => $title) {
                                                    
                                            ?>
                                                <tr>
                                                    <td><?= $title ?></td>
                                                    <td><?= $jarak[$key][0] ?></td>
                                                    <td><?= $jarak[$key][1] ?></td>
                                                </tr>

                                            <?php }} else { ?>
                                                <tr>
                                                    <td colspan="3">Tidak ada Data</td>
                                                </tr>

                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                                <!--//table-responsive-->
                            </div>
                            <!--//app-card-body-->
                        </div>
                        <!--//app-card-->

                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            <div class="app-card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered app-table-hover mb-0 text-left">
                                        <thead>
                                            <tr>
                                                <th class="cell" colspan="4">NILAI PREFERENSI</th>
                                            </tr>
                                            <tr>
                                                <th class="cell">ALTERNATIF</th>
                                                <th class="cell">PREFERENSI (V)</th>
                                                <th class="cell">RANKING</th>
                                                <th class="cell">PERSENTASE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php

                                                if (isset($fetchdata['topsis'])) {
                                                    $preferensi_ringan = $jarak_ringan[1] / array_sum($jarak_ringan);
                                                    $preferensi_sedang = $jarak_sedang[1] / array_sum($jarak_sedang);
                                                    $preferensi_berat = $jarak_berat[1] / array_sum($jarak_berat);
                                                    $preferensi_kritis = $jarak_kritis[1] / array_sum($jarak_kritis);

                                                    $preferensi = [$preferensi_ringan, $preferensi_sedang, $preferensi_berat, $preferensi_kritis];

                                                    $rank = [];

                                                    $ordered_preferensi = $preferensi;
                                                    rsort($ordered_preferensi);

                                                    foreach ($preferensi as $key => $value) {
                                                        foreach ($ordered_preferensi as $ordered_key => $ordered_value) {
                                                            if ($value === $ordered_value) {
                                                                $key = $ordered_key;
                                                                break;
                                                            }
                                                        }
                                                        array_push($rank, $key + 1);
                                                    }

                                                    $persentase = [];

                                                    foreach ($preferensi as $key => $value) {
                                                        $persentase[$key] = $value/array_sum($preferensi)*100;
                                                    }

                                                    $title = ["Gejala Ringan", "Gejala Sedang", "Gejala Berat", "Gejala Kritis"];
                                                    
                                                    foreach ($title as $key => $title) {
                                                    
                                            ?>
                                                <tr>
                                                    <td><?= $title ?></td>
                                                    <td><?= $preferensi[$key] ?></td>
                                                    <td><?= $rank[$key] ?></td>
                                                    <td><?= round($persentase[$key], 2) ?>%</td>
                                                </tr>

                                            <?php }} else { ?>
                                                <tr>
                                                    <td colspan="4">Tidak ada Data</td>
                                                </tr>

                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                                <!--//table-responsive-->
                            </div>
                            <!--//app-card-body-->
                        </div>
                        <!--//app-card-->


                        <div class="app-card app-card-orders-table shadow-sm mb-5">
                            <div class="app-card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered app-table-hover mb-0 text-left">
                                        <thead>
                                            <tr>
                                                <th class="cell" colspan="4">HASIL AKHIR</th>
                                            </tr>
                                            <tr>
                                                <th class="cell">ALTERNATIF</th>
                                                <th class="cell">RANKING</th>
                                                <th class="cell">PERSENTASE</th>
                                                <th class="cell">KETERANGAN</th>
                                                <th class="cell">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php

                                                if (isset($fetchdata['topsis'])) {
                                                    $preferensi_ringan = $jarak_ringan[1] / array_sum($jarak_ringan);
                                                    $preferensi_sedang = $jarak_sedang[1] / array_sum($jarak_sedang);
                                                    $preferensi_berat = $jarak_berat[1] / array_sum($jarak_berat);
                                                    $preferensi_kritis = $jarak_kritis[1] / array_sum($jarak_kritis);

                                                    $preferensi = [$preferensi_ringan, $preferensi_sedang, $preferensi_berat, $preferensi_kritis];

                                                    $rank = [];

                                                    $ordered_preferensi = $preferensi;
                                                    rsort($ordered_preferensi);

                                                    foreach ($preferensi as $key => $value) {
                                                        foreach ($ordered_preferensi as $ordered_key => $ordered_value) {
                                                            if ($value === $ordered_value) {
                                                                $key = $ordered_key;
                                                                break;
                                                            }
                                                        }
                                                        array_push($rank, $key + 1);
                                                    }

                                                    $persentase = [];

                                                    foreach ($preferensi as $key => $value) {
                                                        $persentase[$key] = $value/array_sum($preferensi)*100;
                                                    }

                                                    $keterangan = ["Lakukan Isolasi Mandiri Dirumah","Segera Melakukan Test swab","Konfirmasikan Klinik Terdekat","Segera Laporkan ke Rumah Sakit Rujukan"];

                                                    if ($title[$key] == "Gejala Ringan") {
                                                        $keterangan == [0];
                                                    }
                                                    foreach ($preferensi as $key => $title){

                                                    }

                                                    $title = ["Gejala Ringan", "Gejala Sedang", "Gejala Berat", "Gejala Kritis"];
                                                    
                                                    foreach ($title as $key => $title) {

                                                        $hasil_akhir = [$title, $rank[$key], $keterangan[$key]];   
                                                        
                                                        // foreach ($hasil_akhir as $data){

                                                        // }
                                                         

                                                        if ($rank[$key]== "1") {   
                                                           
                                            ?>
                                                <tr>
                                                    <td><?= $title ?></td>
                                                    <td><?= $rank[$key] ?></td>
                                                    <td><?= round($persentase[$key], 2) ?>%</td>
                                                    <td><?= $keterangan[$key] ?></td>
                                                    <td>

                                                    
                                                            <a href="input_topsis.php?user=<?= $_GET['user'] ?>&title=<?= $title ?>&rank=<?= $rank[$key] ?>&keterangan=<?= $keterangan[$key] ?> " class="btn btn-success">Submit Hasil</a> 
                                                    
                                                    </td>
                                                </tr>

                                            <?php }}} else { ?>
                                                <tr>
                                                    <td colspan="4">Tidak ada Data</td>
                                                </tr>

                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                                <!--//table-responsive-->
                            </div>
                            <!--//app-card-body-->
                        </div>
                        <!--//app-card-->

                    </div>
                    <!--//tab-pane-->
                </div>
                <!--//tab-content-->



            </div>
            <!--//container-fluid-->
        </div>
        <!--//app-content-->

        <footer class="app-footer">
            <div class="container text-center py-3">
                <!--/* This template is free as long as you keep the footer attribution link. If you'd like to use the template without the attribution link, you can buy the commercial license via our website: themes.3rdwavemedia.com Thank you for your support. :) */-->
                <small class="copyright">Copyright by © Puskesmas Jatibening</small>

            </div>
        </footer>
        <!--//app-footer-->

    </div>
    <!--//app-wrapper-->


    <!-- Javascript -->
    <script src="assets/plugins/popper.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>


    <!-- Page Specific JS -->
    <script src="assets/js/app.js"></script>

</body>

</html>