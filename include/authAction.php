<?php
session_start();

require __DIR__.'/vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\Auth;
use Firebase\Auth\Token\Exception\InvalidToken;

$factory = (new Factory)
    ->withServiceAccount('covid19-b37da-firebase-adminsdk-4c9di-3e89b0b700.json')
    ->withDatabaseUri('https://covid19-b37da-default-rtdb.firebaseio.com/');
  
$auth = $factory->createAuth();

// include("db.php"); 

if (isset($_POST['signUp'])) {


    $email = $_POST['signup-email'];
    $password = $_POST['signup-password'];

$userProperties = [
    'email' => $email,
    'emailVerified' => false,
    // 'phoneNumber' => '+15555550100',
    'password' => $password,
    // 'displayName' => 'John Doe',
    // 'photoUrl' => 'http://www.example.com/12345678/photo.png',
    // 'disabled' => false,
];

$createdUser = $auth->createUser($userProperties);
if ($createdUser) {
    // $_SESSION['status'] = "User Created Successfully";
    header("Location: ../login.php");
}else{
    // $_SESSION['status'] = "User Not Create";
    header("Location: ../signup.php");
}

}


if (isset($_POST['login-submit'])) {
    $email = $_POST['signin-email'];
    $clearTextPassword = $_POST['signin-password'];

    try {
        $user = $auth->getUserByEmail("$email");

        $signInResult = $auth->signInWithEmailAndPassword($email, $clearTextPassword);
        $idTokenString = $signInResult->idToken();

        try {
            $verifiedIdToken = $auth->verifyIdToken($idTokenString);
            $uid = $verifiedIdToken->claims()->get('sub');  
            $_SESSION['verified_user_id'] = $uid;
            $_SESSION['idTokenString'] = $idTokenString;
            $_SESSION['isLogin'] = true;

            // $_SESSION['status'] = "You are Logged In Successfully";
            header("Location: ../index.php");
            exit();


        } catch (InvalidToken $e) {
            echo 'The token is invalid: '.$e->getMessage();
        } catch (\InvalidArgumentException $e) {
            echo 'The token could not be parsed: '.$e->getMessage();
        }

    }catch (\Kreait\Firebase\Exception\Auth\UserNotFound $e){
        // echo $e->message();
        // $_SESSION['status'] = "No Email Found";
        header("Location: ../login.php");
        exit();
    }
}