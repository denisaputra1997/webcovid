<?php

require __DIR__.'/vendor/autoload.php';

use Kreait\Firebase\Factory;

$factory = (new Factory)->withDatabaseUri('https://covid19-b37da-default-rtdb.firebaseio.com/');

$database = $factory->createDatabase();
?>