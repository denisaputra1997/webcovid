<?php

    include('include/db.php');

    if (isset($_GET['user'])) {

        $fetchdata = $database->getReference("Data")->getChild($_GET['user'])->getValue();

        if (isset($fetchdata['topsis'])) {

            $gejala_ringan = [0.696310624, 0.588348405, 0.43643578, 0.43643578, 0.377964473, 0.377964473, 0.182574186, 0.39223227, 0.39223227, 0.348155312, 0.324442842, 0.272165527, 0.272165527, 0.272165527, 0.272165527];
            $gejala_sedang = [0.348155312, 0.39223227, 0.43643578, 0.43643578, 0.377964473, 0.377964473, 0.730296743, 0.588348405, 0.588348405, 0.696310624, 0.486664263, 0.40824829, 0.40824829, 0.40824829, 0.40824829];
            $gejala_berat = [0.522232968, 0.588348405, 0.654653671, 0.654653671, 0.755928946, 0.755928946, 0.547722558, 0.588348405, 0.588348405, 0.522232968, 0.648885685, 0.680413817, 0.680413817, 0.544331054, 0.544331054];
            $gejala_kritis = [0.348155312, 0.39223227, 0.43643578, 0.43643578, 0.377964473, 0.377964473, 0.365148372, 0.39223227, 0.39223227, 0.348155312, 0.486664263, 0.544331054, 0.544331054, 0.680413817, 0.680413817];

            $hasil_ringan = [];
            $hasil_sedang = [];
            $hasil_berat = [];
            $hasil_kritis = [];

            
            for ($i=1; $i <= 15; $i++) { 
                $y = $i - 1;
                $hasil_ringan[$i] = $fetchdata["topsis"]["c" . $i] * $gejala_ringan[$y];
                $hasil_sedang[$i] = $fetchdata["topsis"]["c" . $i] * $gejala_sedang[$y];
                $hasil_berat[$i] = $fetchdata["topsis"]["c" . $i] * $gejala_berat[$y];
                $hasil_kritis[$i] = $fetchdata["topsis"]["c" . $i] * $gejala_kritis[$y];
            }

            $solusi_ideal_positif = [];
            $solusi_ideal_negatif = [];
            
            for ($i=1; $i <= 15; $i++) {
                    $tes = [$hasil_ringan[$i], $hasil_sedang[$i], $hasil_berat[$i], $hasil_kritis[$i]];

                    $solusi_ideal_positif[$i] = (max($tes));
                    $solusi_ideal_negatif[$i] = (min($tes));
            }

            $solusi_ideal = ["Solusi Ideal A+", "Solusi Ideal A-"];

            $jarak_ringan_positif = [];
            $jarak_ringan_negatif = [];
            $jarak_sedang_positif = [];
            $jarak_sedang_negatif = [];
            $jarak_berat_positif = [];
            $jarak_berat_negatif = [];
            $jarak_kritis_positif = [];
            $jarak_kritis_negatif = [];

            for ($i=1; $i <= 15; $i++) {
                $jarak_ringan_positif[$i] = pow(($solusi_ideal_positif[$i] - $hasil_ringan[$i]), 2);
                $jarak_ringan_negatif[$i] = pow(($solusi_ideal_negatif[$i] - $hasil_ringan[$i]), 2);
                $jarak_sedang_positif[$i] = pow(($solusi_ideal_positif[$i] - $hasil_sedang[$i]), 2);
                $jarak_sedang_negatif[$i] = pow(($solusi_ideal_negatif[$i] - $hasil_sedang[$i]), 2);
                $jarak_berat_positif[$i] = pow(($solusi_ideal_positif[$i] - $hasil_berat[$i]), 2);
                $jarak_berat_negatif[$i] = pow(($solusi_ideal_negatif[$i] - $hasil_berat[$i]), 2);
                $jarak_kritis_positif[$i] = pow(($solusi_ideal_positif[$i] - $hasil_kritis[$i]), 2);
                $jarak_kritis_negatif[$i] = pow(($solusi_ideal_negatif[$i] - $hasil_kritis[$i]), 2);
            }

            $jarak_ringan = [sqrt(array_sum($jarak_ringan_positif)), sqrt(array_sum($jarak_ringan_negatif))];
            $jarak_sedang = [sqrt(array_sum($jarak_sedang_positif)), sqrt(array_sum($jarak_sedang_negatif))];
            $jarak_berat = [sqrt(array_sum($jarak_berat_positif)), sqrt(array_sum($jarak_berat_negatif))];
            $jarak_kritis = [sqrt(array_sum($jarak_kritis_positif)), sqrt(array_sum($jarak_kritis_negatif))];

            $jarak = [$jarak_ringan, $jarak_sedang, $jarak_berat, $jarak_kritis];

            $preferensi_ringan = $jarak_ringan[1] / array_sum($jarak_ringan);
            $preferensi_sedang = $jarak_sedang[1] / array_sum($jarak_sedang);
            $preferensi_berat = $jarak_berat[1] / array_sum($jarak_berat);
            $preferensi_kritis = $jarak_kritis[1] / array_sum($jarak_kritis);

            $preferensi = [$preferensi_ringan, $preferensi_sedang, $preferensi_berat, $preferensi_kritis];

            $rank = [];

            $ordered_preferensi = $preferensi;
            rsort($ordered_preferensi);

            foreach ($preferensi as $key => $value) {
                foreach ($ordered_preferensi as $ordered_key => $ordered_value) {
                    if ($value === $ordered_value) {
                        $key = $ordered_key;
                        break;
                    }
                }
                array_push($rank, $key + 1);
            }

            $persentase = [];

            foreach ($preferensi as $key => $value) {
                $persentase[$key] = $value/array_sum($preferensi)*100;
            }

        }

        foreach ($rank as $key => $value) {
            if ($value == 1) {
                $gejala = $key;
            }
        }

        switch ($gejala) {
            case '0':
                $data = array("hasil" => "Gejala Ringan");
                break;
            case '1':
                $data = array("hasil" => "Gejala Sedang");
                break;
            case '2':
                $data = array("hasil" => "Gejala Berat");
                break;
            case '3':
                $data = array("hasil" => "Gejala Kritis");
                break;
        }

    } else {
        $data = array("hasil" => null);
    }

    echo json_encode($data);

?>